import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frame;
	private JTextField tfIntervalMinim;
	private JTextField tfIntervalMaxim;
	private JTextField tfServireMinim;
	private JTextField tfServireMaxim;
	private JTextField tfNumarCozi;
	private JTextField tfNumarClienti;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 656, 477);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIntervalMinim = new JLabel("INTERVAL MINIM");
		lblIntervalMinim.setBounds(12, 51, 123, 40);
		frame.getContentPane().add(lblIntervalMinim);
		
		JLabel lblIntervalMaxim = new JLabel("INTERVAL MAXIM");
		lblIntervalMaxim.setBounds(12, 128, 123, 40);
		frame.getContentPane().add(lblIntervalMaxim);
		
		JLabel lblServireMinim = new JLabel("SERVIRE MINIM");
		lblServireMinim.setBounds(312, 51, 123, 40);
		frame.getContentPane().add(lblServireMinim);
		
		JLabel lblServireMaxim = new JLabel("SERVIRE MAXIM");
		lblServireMaxim.setBounds(312, 128, 123, 40);
		frame.getContentPane().add(lblServireMaxim);
		
		tfIntervalMinim = new JTextField();
		tfIntervalMinim.setBounds(136, 60, 116, 22);
		frame.getContentPane().add(tfIntervalMinim);
		tfIntervalMinim.setColumns(10);
		
		tfIntervalMaxim = new JTextField();
		tfIntervalMaxim.setColumns(10);
		tfIntervalMaxim.setBounds(136, 137, 116, 22);
		frame.getContentPane().add(tfIntervalMaxim);
		
		tfServireMinim = new JTextField();
		tfServireMinim.setColumns(10);
		tfServireMinim.setBounds(447, 60, 116, 22);
		frame.getContentPane().add(tfServireMinim);
		
		tfServireMaxim = new JTextField();
		tfServireMaxim.setColumns(10);
		tfServireMaxim.setBounds(447, 137, 116, 22);
		frame.getContentPane().add(tfServireMaxim);
		
		JLabel lblNumarCozi = new JLabel("NUMAR COZI");
		lblNumarCozi.setBounds(152, 207, 123, 40);
		frame.getContentPane().add(lblNumarCozi);
		
		JLabel lblNumarClienti = new JLabel("NUMAR CLIENTI");
		lblNumarClienti.setBounds(152, 260, 123, 40);
		frame.getContentPane().add(lblNumarClienti);
		
		JButton btnSimulare = new JButton("SIMULARE");
		btnSimulare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String intervalMinim,intervalMaxim,servireMinim,servireMaxim,numarCozi,numarClienti;
				int intervalMin,intervalMax,servireMin,servireMax,nrCozi,nrClienti;
				
				intervalMinim=tfIntervalMinim.getText();
				intervalMaxim=tfIntervalMaxim.getText();
				servireMinim=tfServireMinim.getText();
				servireMaxim=tfServireMaxim.getText();
				numarCozi=tfNumarCozi.getText();
				numarClienti=tfNumarClienti.getText();
				
				intervalMin=Integer.parseInt(intervalMinim);
				intervalMax=Integer.parseInt(intervalMaxim);
				servireMin=Integer.parseInt(servireMinim);
				servireMax=Integer.parseInt(servireMaxim);
				nrCozi=Integer.parseInt(numarCozi);
				nrClienti=Integer.parseInt(numarClienti);
				
				Start start = new Start (intervalMin,intervalMax,servireMin,servireMax,nrCozi,nrClienti);
				start.run();
				
			}
		});
		btnSimulare.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSimulare.setBounds(233, 337, 161, 40);
		frame.getContentPane().add(btnSimulare);
		
		tfNumarCozi = new JTextField();
		tfNumarCozi.setColumns(10);
		tfNumarCozi.setBounds(278, 216, 116, 22);
		frame.getContentPane().add(tfNumarCozi);
		
		tfNumarClienti = new JTextField();
		tfNumarClienti.setColumns(10);
		tfNumarClienti.setBounds(278, 269, 116, 22);
		frame.getContentPane().add(tfNumarClienti);
	}
}
