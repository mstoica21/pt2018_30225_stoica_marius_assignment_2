import java.util.ArrayList;
import java.util.LinkedList;

public class Pop extends Thread {
	
	private LinkedList <Client> coadaClienti = new LinkedList <Client> ();
	private ArrayList <Coada> cozi = new ArrayList <Coada> (200);
	
	public Pop (LinkedList <Client> coadaClienti,ArrayList <Coada> cozi )
	{
		this.coadaClienti=coadaClienti;
		this.cozi=cozi;
	}
	
	public void run ()
	{
		int coadaSize=coadaClienti.size();
		for (int i = 0 ; i<coadaSize;i++) 
		{
			Client client = new Client ();
			client=coadaClienti.getFirst();
			coadaClienti.removeFirst();
			try {
				Thread.sleep(client.getIntervalAsteptare());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("Clientul cu id " + client.getId() + " a fost pus in coada secundara!");
			int index=0;
			int minn=cozi.get(0).getCoada().size();
			
			for (int j=0;j<cozi.size();j++)
			{
				Coada coada = new Coada();
				coada=cozi.get(j);
				if (minn>coada.getCoada().size())
				{
					minn=coada.getCoada().size();
					index=j;
				}
			}
			LinkedList <Client> queue = new LinkedList <Client> ();
			queue=cozi.get(index).getCoada();
			queue.add(client);
			cozi.get(index).setCoada(queue);
		}
	}
	
	public ArrayList <Coada> returnCoadaSecundara ()
	{
		return cozi;
	}
	
	
}
