import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class List extends Thread{
	
	private LinkedList <Client> coadaClienti = new LinkedList <Client> ();
	
	
	private int nrClienti;
	private int intervalLow;
	private int intervalHigh;
	private int servireLow;
	private int servireHigh;
	
	public List (int nrClienti, int intervalLow,int intervalHigh,int servireLow,int servireHigh)
	{
		this.nrClienti=nrClienti;
		this.intervalLow=intervalLow;
		this.intervalHigh=intervalHigh;
		this.servireLow=servireLow;
		this.servireHigh=servireHigh;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		for (int i=0; i< nrClienti;i++)
		{
			Random rand1 = new Random ();
			int timpServire = rand1.nextInt(servireHigh-servireLow) + servireLow;
			Random rand2 = new Random ();
			int timpAsteptare=rand2.nextInt(intervalHigh-intervalLow) + intervalLow;
			Client c = new Client (i,timpAsteptare*1000,timpServire*1000);
			coadaClienti.add(c);
			
			
			
			System.out.println("A fost creat clientul cu id-ul " + i );
		}
		
		
	}
	
	public LinkedList <Client> returnList ()
	{
		return coadaClienti;
	}

}
