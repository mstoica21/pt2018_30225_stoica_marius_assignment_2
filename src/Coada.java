import java.util.LinkedList;

public class Coada extends Thread {
		
		private LinkedList <Client> coada = new LinkedList <Client> ();
		private LinkedList <Client> queue = new LinkedList <Client> ();
		private int nrCoada;
		private int queueSize;
		
		public LinkedList<Client> getCoada() {
			return coada;
		}

		public void setCoada(LinkedList<Client> coada) {
			this.coada = coada;
		}
		
		public Coada (LinkedList <Client> queue,int nrCoada,int queueSize)
		{
			this.queue=queue;
			this.nrCoada=nrCoada;
			this.queueSize=queueSize;
		}

		public Coada ()
		{
			
		}
		
		public void run ()
		{
			for (int i=0;i<queueSize;i++)
			{
				Client client = new Client ();
				client=queue.getFirst();
				try {
					Thread.sleep(client.getIntervalServire());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Clientul cu id-ul " + client.getId() + " a parasit coada " + nrCoada);
				queue.removeFirst();
			}
		}
}
