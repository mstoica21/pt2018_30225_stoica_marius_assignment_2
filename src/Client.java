
public class Client {
	
	private int id;
	private int intervalAsteptare;
	private int intervalServire;
	

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIntervalAsteptare() {
		return intervalAsteptare;
	}

	public void setIntervalAsteptare(int intervalAsteptare) {
		this.intervalAsteptare = intervalAsteptare;
	}

	public int getIntervalServire() {
		return intervalServire;
	}

	public void setIntervalServire(int intervalServire) {
		this.intervalServire = intervalServire;
	}

	public Client (int id,int intervalAsteptare,int intervalServire)
	{
		this.id=id;
		this.intervalAsteptare=intervalAsteptare;
		this.intervalServire=intervalServire;
	}
	

	public Client ()
	{
		
	}
	
}
