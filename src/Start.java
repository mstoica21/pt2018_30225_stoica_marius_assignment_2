import java.util.ArrayList;
import java.util.LinkedList;

public class Start {
	
	private static LinkedList <Client> coadaClienti = new LinkedList <Client> ();
	private static ArrayList <Coada> cozi = new ArrayList <Coada> (200);
	
	private int intervalMin,intervalMax,servireMin,servireMax,nrCozi,nrClienti;
	
	public Start (int intervalMin,int intervalMax,int servireMin,int servireMax,int nrCozi,int nrClienti)
	{
		this.intervalMin=intervalMin;
		this.intervalMax=intervalMax;
		this.servireMin=servireMin;
		this.servireMax=servireMax;
		this.nrCozi=nrCozi;
		this.nrClienti=nrClienti;
	}
	
	public void run ()
	{
		List creareCoada = new List (nrClienti,intervalMin,intervalMax,servireMin,servireMax);
		Thread t1 = new Thread(creareCoada);
		t1.start();
	
		
		try {
			t1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//creare coada clienti
		
		coadaClienti=creareCoada.returnList();
		
		
		
		for (int i=0;i<coadaClienti.size();i++)
		{
			Client c = new Client ();
			c=coadaClienti.get(i);
			System.out.println(c.getId() + "   " + c.getIntervalAsteptare() + "    " + c.getIntervalServire());
		}
		
		Cozi creareCozi = new Cozi (nrCozi);
		Thread t2 = new Thread(creareCozi);
		t2.start();
		
		try {
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		cozi=creareCozi.returnCozi();
		
		
		//am creat coada principala de clienti
		//am creat cozile secundare de clienti care sunt goale
		
		//adauga client in cozile secundare si il elimina din coada principala 
		Pop pop = new Pop (coadaClienti,cozi);
		Thread t3 = new Thread (pop);
		cozi=pop.returnCoadaSecundara();
		
		t3.start();
		
		try {
			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//elimina clientii din cozile secundare
		
		for (int i=0;i<cozi.size();i++)
		{	
			Coada threadCoada = new Coada(cozi.get(i).getCoada(),i,cozi.get(i).getCoada().size());
			Thread t4 = new Thread (threadCoada);
			t4.start();
		}
	} 
}
